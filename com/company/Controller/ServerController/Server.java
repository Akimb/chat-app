package com.company.Controller.ServerController;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Kompas on 2016-10-20.
 */
public class Server {
    private ServerSocket server;
    public ArrayList<Socket> clientSocket = new ArrayList<>();
    private ServerClientHandler serverClientHandler = new ServerClientHandler();

    @Override
    public String toString() {
        return "Server{}";
    }


    public Server()  {
        try {
            server = new ServerSocket(667);
            System.out.println("Server working");
            while(true) {
                Socket client = server.accept();
                clientSocket.add(client);
                System.out.println("Client connected', delegating him to seperate thread");
                serverClientHandler.deliverClientInfo(client, clientSocket);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public static void main(String[] args){
        new Server();
    }
}
