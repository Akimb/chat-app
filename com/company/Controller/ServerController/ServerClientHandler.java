package com.company.Controller.ServerController;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Kompas on 2016-10-20.
 */
public class ServerClientHandler {
    private PrintWriter writer;
    private InputStream is;
    private BufferedReader bfr;
    private InputStream isr;
    private OutputStream os;
    public Socket sock;
    public ArrayList<Socket> clients;

    public void deliverClientInfo(Socket sock, ArrayList<Socket> clients){
        try {
            is = sock.getInputStream();
            this.sock = sock;
            this.clients = clients;
            bfr = new BufferedReader(new InputStreamReader((is)));

            System.out.println("Safely passed client socket.");
                try {
                    String line;
                    while((line = bfr.readLine()) != null) {
                        sendInfoAllClients(line);
                        Thread.sleep(10);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendInfoAllClients(String line) throws IOException {
        for(int i = 0; i < clients.size(); i++) {
                PrintWriter pr  = new PrintWriter(new OutputStreamWriter(clients.get(i).getOutputStream()));
                pr.println(line);
                pr.flush();
        }
    }
}
